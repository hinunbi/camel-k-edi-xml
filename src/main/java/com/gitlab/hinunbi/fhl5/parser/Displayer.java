/* -----------------------------------------------------------------------------
 * Displayer.java
 * -----------------------------------------------------------------------------
 *
 * Producer : com.parse2.aparse.Parser 2.5
 * Produced : Wed Dec 02 21:11:29 KST 2020
 *
 * -----------------------------------------------------------------------------
 */

package com.gitlab.hinunbi.fhl5.parser;

import java.util.ArrayList;

public class Displayer implements Visitor
{

  public Object visit(Rule_FHL5 rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_StandardMessageIdentification rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_MasterAWBConsignmentDetail rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_MasterAWBIdentification rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_AWBOriginAndDestination rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_QuantityDetail rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_HousWaybillSummaryDetails rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_HouseWaybillOriginAndDestination rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_HouseWaybillTotals rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_NatureOfGoods rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_SpecialHandlingRequirements rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_FreeTextDescriptionOfGoods rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_HarmonisedTariffScheduleInformation rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_OtherInformation rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ShipperNameAndAddress rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ShipperName rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ShipperStreetAddress rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ShipperLocation rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ShipperCodedLocation rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ShipperContactDetail rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ConsigneeNameAndAddress rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ConsigneeName rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ConsigneeStreetAddress rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ConsigneeLocation rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ConsigneeCodedLocation rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ConsigneeContactDetail rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ChargeDeclarations rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_PrepaidCollectChargeDeclarations rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ValueForCarriageDeclaration rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ValueForCustomsDeclaration rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ValueForInsuranceDeclaration rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_AirlinePrefix rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_AWBSerialNumber rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ContactIdentifier rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ContactNumber rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ControlInformationIdentifier rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_DeclaredValueForCarriage rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_DeclaredValueForCustoms rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_DeclaredValueForInsurance rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_DepartureCode rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_DestinationCode rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_FreeText rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_HarmonisedCommodityCode rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_HWBSerialNumber rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_InformationIdentifier rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ADR rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_LOC rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_NAM rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ISOCountryCode rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ISOCurrencyCode rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_MBI rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_CNE rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_CVD rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_HBS rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_HTS rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_OCI rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_SHP rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_TXT rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ManifestDescriptionOfGoods rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_Name rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_NoCustomsValue rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_NoValue rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_NoValueDeclared rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_NumberOfPieces rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_OriginCode rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_PCIndOtherCharges rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_PCIndWeightValuation rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_Place rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_PostCode rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_ShipmentDescriptionCode rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_SLAC rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_SpecialHandlingCode rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_StateProvince rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_StreetAddress rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_SupplementaryControlInformation rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_Weight rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_WeightCode rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_CRLF rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_CR rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_LF rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_Slant rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_Hyphen rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_Alpha rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_Numeric rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_Decimal rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_Mixed rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Rule_Text rule)
  {
    return visitRules(rule.rules);
  }

  public Object visit(Terminal_StringValue value)
  {
    System.out.print(value.spelling);
    return null;
  }

  public Object visit(Terminal_NumericValue value)
  {
    System.out.print(value.spelling);
    return null;
  }

  private Object visitRules(ArrayList<Rule> rules)
  {
    for (Rule rule : rules)
      rule.accept(this);
    return null;
  }
}

/* -----------------------------------------------------------------------------
 * eof
 * -----------------------------------------------------------------------------
 */
