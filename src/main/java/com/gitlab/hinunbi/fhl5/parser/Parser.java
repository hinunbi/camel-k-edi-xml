/* -----------------------------------------------------------------------------
 * Parser.java
 * -----------------------------------------------------------------------------
 *
 * Producer : com.parse2.aparse.Parser 2.5
 * Produced : Wed Dec 02 21:11:29 KST 2020
 *
 * -----------------------------------------------------------------------------
 */

package com.gitlab.hinunbi.fhl5.parser;

import java.util.Stack;
import java.util.Properties;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.IOException;

public class Parser
{
  private Parser() {}

  static public void main(String[] args)
  {
    Properties arguments = new Properties();
    String error = "";
    boolean ok = args.length > 0;

    if (ok)
    {
      arguments.setProperty("Trace", "Off");
      arguments.setProperty("Rule", "FHL5");

      for (int i = 0; i < args.length; i++)
      {
        if (args[i].equals("-trace"))
          arguments.setProperty("Trace", "On");
        else if (args[i].equals("-visitor"))
          arguments.setProperty("Visitor", args[++i]);
        else if (args[i].equals("-file"))
          arguments.setProperty("File", args[++i]);
        else if (args[i].equals("-string"))
          arguments.setProperty("String", args[++i]);
        else if (args[i].equals("-rule"))
          arguments.setProperty("Rule", args[++i]);
        else
        {
          error = "unknown argument: " + args[i];
          ok = false;
        }
      }
    }

    if (ok)
    {
      if (arguments.getProperty("File") == null &&
          arguments.getProperty("String") == null)
      {
        error = "insufficient arguments: -file or -string required";
        ok = false;
      }
    }

    if (!ok)
    {
      System.out.println("error: " + error);
      System.out.println("usage: Parser [-rule rulename] [-trace] <-file file | -string string> [-visitor visitor]");
    }
    else
    {
      try
      {
        Rule rule = null;

        if (arguments.getProperty("File") != null)
        {
          rule = 
            parse(
              arguments.getProperty("Rule"), 
              new File(arguments.getProperty("File")), 
              arguments.getProperty("Trace").equals("On"));
        }
        else if (arguments.getProperty("String") != null)
        {
          rule = 
            parse(
              arguments.getProperty("Rule"), 
              arguments.getProperty("String"), 
              arguments.getProperty("Trace").equals("On"));
        }

        if (arguments.getProperty("Visitor") != null)
        {
          Visitor visitor = 
            (Visitor)Class.forName(arguments.getProperty("Visitor")).newInstance();
          rule.accept(visitor);
        }
      }
      catch (IllegalArgumentException e)
      {
        System.out.println("argument error: " + e.getMessage());
      }
      catch (IOException e)
      {
        System.out.println("io error: " + e.getMessage());
      }
      catch (ParserException e)
      {
        System.out.println("parser error: " + e.getMessage());
      }
      catch (ClassNotFoundException e)
      {
        System.out.println("visitor error: class not found - " + e.getMessage());
      }
      catch (IllegalAccessException e)
      {
        System.out.println("visitor error: illegal access - " + e.getMessage());
      }
      catch (InstantiationException e)
      {
        System.out.println("visitor error: instantiation failure - " + e.getMessage());
      }
    }
  }

  static public Rule parse(String rulename, String string)
  throws IllegalArgumentException,
         ParserException
  {
    return parse(rulename, string, false);
  }

  static public Rule parse(String rulename, InputStream in)
  throws IllegalArgumentException,
         IOException,
         ParserException
  {
    return parse(rulename, in, false);
  }

  static public Rule parse(String rulename, File file)
  throws IllegalArgumentException,
         IOException,
         ParserException
  {
    return parse(rulename, file, false);
  }

  static private Rule parse(String rulename, String string, boolean trace)
  throws IllegalArgumentException,
         ParserException
  {
    if (rulename == null)
      throw new IllegalArgumentException("null rulename");
    if (string == null)
      throw new IllegalArgumentException("null string");

    ParserContext context = new ParserContext(string, trace);

    Rule rule = null;
    if (rulename.equalsIgnoreCase("FHL5")) rule = Rule_FHL5.parse(context);
    else if (rulename.equalsIgnoreCase("StandardMessageIdentification")) rule = Rule_StandardMessageIdentification.parse(context);
    else if (rulename.equalsIgnoreCase("MasterAWBConsignmentDetail")) rule = Rule_MasterAWBConsignmentDetail.parse(context);
    else if (rulename.equalsIgnoreCase("MasterAWBIdentification")) rule = Rule_MasterAWBIdentification.parse(context);
    else if (rulename.equalsIgnoreCase("AWBOriginAndDestination")) rule = Rule_AWBOriginAndDestination.parse(context);
    else if (rulename.equalsIgnoreCase("QuantityDetail")) rule = Rule_QuantityDetail.parse(context);
    else if (rulename.equalsIgnoreCase("HousWaybillSummaryDetails")) rule = Rule_HousWaybillSummaryDetails.parse(context);
    else if (rulename.equalsIgnoreCase("HouseWaybillOriginAndDestination")) rule = Rule_HouseWaybillOriginAndDestination.parse(context);
    else if (rulename.equalsIgnoreCase("HouseWaybillTotals")) rule = Rule_HouseWaybillTotals.parse(context);
    else if (rulename.equalsIgnoreCase("NatureOfGoods")) rule = Rule_NatureOfGoods.parse(context);
    else if (rulename.equalsIgnoreCase("SpecialHandlingRequirements")) rule = Rule_SpecialHandlingRequirements.parse(context);
    else if (rulename.equalsIgnoreCase("FreeTextDescriptionOfGoods")) rule = Rule_FreeTextDescriptionOfGoods.parse(context);
    else if (rulename.equalsIgnoreCase("HarmonisedTariffScheduleInformation")) rule = Rule_HarmonisedTariffScheduleInformation.parse(context);
    else if (rulename.equalsIgnoreCase("OtherInformation")) rule = Rule_OtherInformation.parse(context);
    else if (rulename.equalsIgnoreCase("ShipperNameAndAddress")) rule = Rule_ShipperNameAndAddress.parse(context);
    else if (rulename.equalsIgnoreCase("ShipperName")) rule = Rule_ShipperName.parse(context);
    else if (rulename.equalsIgnoreCase("ShipperStreetAddress")) rule = Rule_ShipperStreetAddress.parse(context);
    else if (rulename.equalsIgnoreCase("ShipperLocation")) rule = Rule_ShipperLocation.parse(context);
    else if (rulename.equalsIgnoreCase("ShipperCodedLocation")) rule = Rule_ShipperCodedLocation.parse(context);
    else if (rulename.equalsIgnoreCase("ShipperContactDetail")) rule = Rule_ShipperContactDetail.parse(context);
    else if (rulename.equalsIgnoreCase("ConsigneeNameAndAddress")) rule = Rule_ConsigneeNameAndAddress.parse(context);
    else if (rulename.equalsIgnoreCase("ConsigneeName")) rule = Rule_ConsigneeName.parse(context);
    else if (rulename.equalsIgnoreCase("ConsigneeStreetAddress")) rule = Rule_ConsigneeStreetAddress.parse(context);
    else if (rulename.equalsIgnoreCase("ConsigneeLocation")) rule = Rule_ConsigneeLocation.parse(context);
    else if (rulename.equalsIgnoreCase("ConsigneeCodedLocation")) rule = Rule_ConsigneeCodedLocation.parse(context);
    else if (rulename.equalsIgnoreCase("ConsigneeContactDetail")) rule = Rule_ConsigneeContactDetail.parse(context);
    else if (rulename.equalsIgnoreCase("ChargeDeclarations")) rule = Rule_ChargeDeclarations.parse(context);
    else if (rulename.equalsIgnoreCase("PrepaidCollectChargeDeclarations")) rule = Rule_PrepaidCollectChargeDeclarations.parse(context);
    else if (rulename.equalsIgnoreCase("ValueForCarriageDeclaration")) rule = Rule_ValueForCarriageDeclaration.parse(context);
    else if (rulename.equalsIgnoreCase("ValueForCustomsDeclaration")) rule = Rule_ValueForCustomsDeclaration.parse(context);
    else if (rulename.equalsIgnoreCase("ValueForInsuranceDeclaration")) rule = Rule_ValueForInsuranceDeclaration.parse(context);
    else if (rulename.equalsIgnoreCase("AirlinePrefix")) rule = Rule_AirlinePrefix.parse(context);
    else if (rulename.equalsIgnoreCase("AWBSerialNumber")) rule = Rule_AWBSerialNumber.parse(context);
    else if (rulename.equalsIgnoreCase("ContactIdentifier")) rule = Rule_ContactIdentifier.parse(context);
    else if (rulename.equalsIgnoreCase("ContactNumber")) rule = Rule_ContactNumber.parse(context);
    else if (rulename.equalsIgnoreCase("ControlInformationIdentifier")) rule = Rule_ControlInformationIdentifier.parse(context);
    else if (rulename.equalsIgnoreCase("DeclaredValueForCarriage")) rule = Rule_DeclaredValueForCarriage.parse(context);
    else if (rulename.equalsIgnoreCase("DeclaredValueForCustoms")) rule = Rule_DeclaredValueForCustoms.parse(context);
    else if (rulename.equalsIgnoreCase("DeclaredValueForInsurance")) rule = Rule_DeclaredValueForInsurance.parse(context);
    else if (rulename.equalsIgnoreCase("DepartureCode")) rule = Rule_DepartureCode.parse(context);
    else if (rulename.equalsIgnoreCase("DestinationCode")) rule = Rule_DestinationCode.parse(context);
    else if (rulename.equalsIgnoreCase("FreeText")) rule = Rule_FreeText.parse(context);
    else if (rulename.equalsIgnoreCase("HarmonisedCommodityCode")) rule = Rule_HarmonisedCommodityCode.parse(context);
    else if (rulename.equalsIgnoreCase("HWBSerialNumber")) rule = Rule_HWBSerialNumber.parse(context);
    else if (rulename.equalsIgnoreCase("InformationIdentifier")) rule = Rule_InformationIdentifier.parse(context);
    else if (rulename.equalsIgnoreCase("ADR")) rule = Rule_ADR.parse(context);
    else if (rulename.equalsIgnoreCase("LOC")) rule = Rule_LOC.parse(context);
    else if (rulename.equalsIgnoreCase("NAM")) rule = Rule_NAM.parse(context);
    else if (rulename.equalsIgnoreCase("ISOCountryCode")) rule = Rule_ISOCountryCode.parse(context);
    else if (rulename.equalsIgnoreCase("ISOCurrencyCode")) rule = Rule_ISOCurrencyCode.parse(context);
    else if (rulename.equalsIgnoreCase("MBI")) rule = Rule_MBI.parse(context);
    else if (rulename.equalsIgnoreCase("CNE")) rule = Rule_CNE.parse(context);
    else if (rulename.equalsIgnoreCase("CVD")) rule = Rule_CVD.parse(context);
    else if (rulename.equalsIgnoreCase("HBS")) rule = Rule_HBS.parse(context);
    else if (rulename.equalsIgnoreCase("HTS")) rule = Rule_HTS.parse(context);
    else if (rulename.equalsIgnoreCase("OCI")) rule = Rule_OCI.parse(context);
    else if (rulename.equalsIgnoreCase("SHP")) rule = Rule_SHP.parse(context);
    else if (rulename.equalsIgnoreCase("TXT")) rule = Rule_TXT.parse(context);
    else if (rulename.equalsIgnoreCase("ManifestDescriptionOfGoods")) rule = Rule_ManifestDescriptionOfGoods.parse(context);
    else if (rulename.equalsIgnoreCase("Name")) rule = Rule_Name.parse(context);
    else if (rulename.equalsIgnoreCase("NoCustomsValue")) rule = Rule_NoCustomsValue.parse(context);
    else if (rulename.equalsIgnoreCase("NoValue")) rule = Rule_NoValue.parse(context);
    else if (rulename.equalsIgnoreCase("NoValueDeclared")) rule = Rule_NoValueDeclared.parse(context);
    else if (rulename.equalsIgnoreCase("NumberOfPieces")) rule = Rule_NumberOfPieces.parse(context);
    else if (rulename.equalsIgnoreCase("OriginCode")) rule = Rule_OriginCode.parse(context);
    else if (rulename.equalsIgnoreCase("PCIndOtherCharges")) rule = Rule_PCIndOtherCharges.parse(context);
    else if (rulename.equalsIgnoreCase("PCIndWeightValuation")) rule = Rule_PCIndWeightValuation.parse(context);
    else if (rulename.equalsIgnoreCase("Place")) rule = Rule_Place.parse(context);
    else if (rulename.equalsIgnoreCase("PostCode")) rule = Rule_PostCode.parse(context);
    else if (rulename.equalsIgnoreCase("ShipmentDescriptionCode")) rule = Rule_ShipmentDescriptionCode.parse(context);
    else if (rulename.equalsIgnoreCase("SLAC")) rule = Rule_SLAC.parse(context);
    else if (rulename.equalsIgnoreCase("SpecialHandlingCode")) rule = Rule_SpecialHandlingCode.parse(context);
    else if (rulename.equalsIgnoreCase("StateProvince")) rule = Rule_StateProvince.parse(context);
    else if (rulename.equalsIgnoreCase("StreetAddress")) rule = Rule_StreetAddress.parse(context);
    else if (rulename.equalsIgnoreCase("SupplementaryControlInformation")) rule = Rule_SupplementaryControlInformation.parse(context);
    else if (rulename.equalsIgnoreCase("Weight")) rule = Rule_Weight.parse(context);
    else if (rulename.equalsIgnoreCase("WeightCode")) rule = Rule_WeightCode.parse(context);
    else if (rulename.equalsIgnoreCase("CRLF")) rule = Rule_CRLF.parse(context);
    else if (rulename.equalsIgnoreCase("CR")) rule = Rule_CR.parse(context);
    else if (rulename.equalsIgnoreCase("LF")) rule = Rule_LF.parse(context);
    else if (rulename.equalsIgnoreCase("Slant")) rule = Rule_Slant.parse(context);
    else if (rulename.equalsIgnoreCase("Hyphen")) rule = Rule_Hyphen.parse(context);
    else if (rulename.equalsIgnoreCase("Alpha")) rule = Rule_Alpha.parse(context);
    else if (rulename.equalsIgnoreCase("Numeric")) rule = Rule_Numeric.parse(context);
    else if (rulename.equalsIgnoreCase("Decimal")) rule = Rule_Decimal.parse(context);
    else if (rulename.equalsIgnoreCase("Mixed")) rule = Rule_Mixed.parse(context);
    else if (rulename.equalsIgnoreCase("Text")) rule = Rule_Text.parse(context);
    else throw new IllegalArgumentException("unknown rule");

    if (rule == null)
    {
      throw new ParserException(
        "rule \"" + (String)context.getErrorStack().peek() + "\" failed",
        context.text,
        context.getErrorIndex(),
        context.getErrorStack());
    }

    if (context.text.length() > context.index)
    {
      ParserException primaryError = 
        new ParserException(
          "extra data found",
          context.text,
          context.index,
          new Stack<String>());

      if (context.getErrorIndex() > context.index)
      {
        ParserException secondaryError = 
          new ParserException(
            "rule \"" + (String)context.getErrorStack().peek() + "\" failed",
            context.text,
            context.getErrorIndex(),
            context.getErrorStack());

        primaryError.initCause(secondaryError);
      }

      throw primaryError;
    }

    return rule;
  }

  static private Rule parse(String rulename, InputStream in, boolean trace)
  throws IllegalArgumentException,
         IOException,
         ParserException
  {
    if (rulename == null)
      throw new IllegalArgumentException("null rulename");
    if (in == null)
      throw new IllegalArgumentException("null input stream");

    int ch = 0;
    StringBuffer out = new StringBuffer();
    while ((ch = in.read()) != -1)
      out.append((char)ch);

    return parse(rulename, out.toString(), trace);
  }

  static private Rule parse(String rulename, File file, boolean trace)
  throws IllegalArgumentException,
         IOException,
         ParserException
  {
    if (rulename == null)
      throw new IllegalArgumentException("null rulename");
    if (file == null)
      throw new IllegalArgumentException("null file");

    BufferedReader in = new BufferedReader(new FileReader(file));
    int ch = 0;
    StringBuffer out = new StringBuffer();
    while ((ch = in.read()) != -1)
      out.append((char)ch);

    in.close();

    return parse(rulename, out.toString(), trace);
  }
}

/* -----------------------------------------------------------------------------
 * eof
 * -----------------------------------------------------------------------------
 */
