/* -----------------------------------------------------------------------------
 * Visitor.java
 * -----------------------------------------------------------------------------
 *
 * Producer : com.parse2.aparse.Parser 2.5
 * Produced : Wed Dec 02 21:11:29 KST 2020
 *
 * -----------------------------------------------------------------------------
 */

package com.gitlab.hinunbi.fhl5.parser;

public interface Visitor
{
  public Object visit(Rule_FHL5 rule);
  public Object visit(Rule_StandardMessageIdentification rule);
  public Object visit(Rule_MasterAWBConsignmentDetail rule);
  public Object visit(Rule_MasterAWBIdentification rule);
  public Object visit(Rule_AWBOriginAndDestination rule);
  public Object visit(Rule_QuantityDetail rule);
  public Object visit(Rule_HousWaybillSummaryDetails rule);
  public Object visit(Rule_HouseWaybillOriginAndDestination rule);
  public Object visit(Rule_HouseWaybillTotals rule);
  public Object visit(Rule_NatureOfGoods rule);
  public Object visit(Rule_SpecialHandlingRequirements rule);
  public Object visit(Rule_FreeTextDescriptionOfGoods rule);
  public Object visit(Rule_HarmonisedTariffScheduleInformation rule);
  public Object visit(Rule_OtherInformation rule);
  public Object visit(Rule_ShipperNameAndAddress rule);
  public Object visit(Rule_ShipperName rule);
  public Object visit(Rule_ShipperStreetAddress rule);
  public Object visit(Rule_ShipperLocation rule);
  public Object visit(Rule_ShipperCodedLocation rule);
  public Object visit(Rule_ShipperContactDetail rule);
  public Object visit(Rule_ConsigneeNameAndAddress rule);
  public Object visit(Rule_ConsigneeName rule);
  public Object visit(Rule_ConsigneeStreetAddress rule);
  public Object visit(Rule_ConsigneeLocation rule);
  public Object visit(Rule_ConsigneeCodedLocation rule);
  public Object visit(Rule_ConsigneeContactDetail rule);
  public Object visit(Rule_ChargeDeclarations rule);
  public Object visit(Rule_PrepaidCollectChargeDeclarations rule);
  public Object visit(Rule_ValueForCarriageDeclaration rule);
  public Object visit(Rule_ValueForCustomsDeclaration rule);
  public Object visit(Rule_ValueForInsuranceDeclaration rule);
  public Object visit(Rule_AirlinePrefix rule);
  public Object visit(Rule_AWBSerialNumber rule);
  public Object visit(Rule_ContactIdentifier rule);
  public Object visit(Rule_ContactNumber rule);
  public Object visit(Rule_ControlInformationIdentifier rule);
  public Object visit(Rule_DeclaredValueForCarriage rule);
  public Object visit(Rule_DeclaredValueForCustoms rule);
  public Object visit(Rule_DeclaredValueForInsurance rule);
  public Object visit(Rule_DepartureCode rule);
  public Object visit(Rule_DestinationCode rule);
  public Object visit(Rule_FreeText rule);
  public Object visit(Rule_HarmonisedCommodityCode rule);
  public Object visit(Rule_HWBSerialNumber rule);
  public Object visit(Rule_InformationIdentifier rule);
  public Object visit(Rule_ADR rule);
  public Object visit(Rule_LOC rule);
  public Object visit(Rule_NAM rule);
  public Object visit(Rule_ISOCountryCode rule);
  public Object visit(Rule_ISOCurrencyCode rule);
  public Object visit(Rule_MBI rule);
  public Object visit(Rule_CNE rule);
  public Object visit(Rule_CVD rule);
  public Object visit(Rule_HBS rule);
  public Object visit(Rule_HTS rule);
  public Object visit(Rule_OCI rule);
  public Object visit(Rule_SHP rule);
  public Object visit(Rule_TXT rule);
  public Object visit(Rule_ManifestDescriptionOfGoods rule);
  public Object visit(Rule_Name rule);
  public Object visit(Rule_NoCustomsValue rule);
  public Object visit(Rule_NoValue rule);
  public Object visit(Rule_NoValueDeclared rule);
  public Object visit(Rule_NumberOfPieces rule);
  public Object visit(Rule_OriginCode rule);
  public Object visit(Rule_PCIndOtherCharges rule);
  public Object visit(Rule_PCIndWeightValuation rule);
  public Object visit(Rule_Place rule);
  public Object visit(Rule_PostCode rule);
  public Object visit(Rule_ShipmentDescriptionCode rule);
  public Object visit(Rule_SLAC rule);
  public Object visit(Rule_SpecialHandlingCode rule);
  public Object visit(Rule_StateProvince rule);
  public Object visit(Rule_StreetAddress rule);
  public Object visit(Rule_SupplementaryControlInformation rule);
  public Object visit(Rule_Weight rule);
  public Object visit(Rule_WeightCode rule);
  public Object visit(Rule_CRLF rule);
  public Object visit(Rule_CR rule);
  public Object visit(Rule_LF rule);
  public Object visit(Rule_Slant rule);
  public Object visit(Rule_Hyphen rule);
  public Object visit(Rule_Alpha rule);
  public Object visit(Rule_Numeric rule);
  public Object visit(Rule_Decimal rule);
  public Object visit(Rule_Mixed rule);
  public Object visit(Rule_Text rule);

  public Object visit(Terminal_StringValue value);
  public Object visit(Terminal_NumericValue value);
}

/* -----------------------------------------------------------------------------
 * eof
 * -----------------------------------------------------------------------------
 */
