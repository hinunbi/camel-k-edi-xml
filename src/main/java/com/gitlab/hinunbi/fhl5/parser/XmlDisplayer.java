/* -----------------------------------------------------------------------------
 * XmlDisplayer.java
 * -----------------------------------------------------------------------------
 *
 * Producer : com.parse2.aparse.Parser 2.5
 * Produced : Wed Dec 02 21:11:29 KST 2020
 *
 * -----------------------------------------------------------------------------
 */

package com.gitlab.hinunbi.fhl5.parser;

import java.util.ArrayList;

public class XmlDisplayer implements Visitor
{
  protected boolean terminal = true;

  public Object visit(Rule_FHL5 rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<FHL5>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</FHL5>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_StandardMessageIdentification rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<StandardMessageIdentification>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</StandardMessageIdentification>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_MasterAWBConsignmentDetail rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<MasterAWBConsignmentDetail>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</MasterAWBConsignmentDetail>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_MasterAWBIdentification rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<MasterAWBIdentification>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</MasterAWBIdentification>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_AWBOriginAndDestination rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<AWBOriginAndDestination>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</AWBOriginAndDestination>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_QuantityDetail rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<QuantityDetail>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</QuantityDetail>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_HousWaybillSummaryDetails rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<HousWaybillSummaryDetails>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</HousWaybillSummaryDetails>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_HouseWaybillOriginAndDestination rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<HouseWaybillOriginAndDestination>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</HouseWaybillOriginAndDestination>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_HouseWaybillTotals rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<HouseWaybillTotals>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</HouseWaybillTotals>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_NatureOfGoods rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<NatureOfGoods>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</NatureOfGoods>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_SpecialHandlingRequirements rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<SpecialHandlingRequirements>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</SpecialHandlingRequirements>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_FreeTextDescriptionOfGoods rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<FreeTextDescriptionOfGoods>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</FreeTextDescriptionOfGoods>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_HarmonisedTariffScheduleInformation rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<HarmonisedTariffScheduleInformation>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</HarmonisedTariffScheduleInformation>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_OtherInformation rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<OtherInformation>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</OtherInformation>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ShipperNameAndAddress rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ShipperNameAndAddress>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ShipperNameAndAddress>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ShipperName rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ShipperName>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ShipperName>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ShipperStreetAddress rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ShipperStreetAddress>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ShipperStreetAddress>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ShipperLocation rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ShipperLocation>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ShipperLocation>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ShipperCodedLocation rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ShipperCodedLocation>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ShipperCodedLocation>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ShipperContactDetail rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ShipperContactDetail>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ShipperContactDetail>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ConsigneeNameAndAddress rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ConsigneeNameAndAddress>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ConsigneeNameAndAddress>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ConsigneeName rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ConsigneeName>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ConsigneeName>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ConsigneeStreetAddress rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ConsigneeStreetAddress>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ConsigneeStreetAddress>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ConsigneeLocation rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ConsigneeLocation>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ConsigneeLocation>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ConsigneeCodedLocation rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ConsigneeCodedLocation>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ConsigneeCodedLocation>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ConsigneeContactDetail rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ConsigneeContactDetail>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ConsigneeContactDetail>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ChargeDeclarations rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ChargeDeclarations>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ChargeDeclarations>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_PrepaidCollectChargeDeclarations rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<PrepaidCollectChargeDeclarations>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</PrepaidCollectChargeDeclarations>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ValueForCarriageDeclaration rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ValueForCarriageDeclaration>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ValueForCarriageDeclaration>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ValueForCustomsDeclaration rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ValueForCustomsDeclaration>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ValueForCustomsDeclaration>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ValueForInsuranceDeclaration rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ValueForInsuranceDeclaration>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ValueForInsuranceDeclaration>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_AirlinePrefix rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<AirlinePrefix>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</AirlinePrefix>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_AWBSerialNumber rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<AWBSerialNumber>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</AWBSerialNumber>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ContactIdentifier rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ContactIdentifier>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ContactIdentifier>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ContactNumber rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ContactNumber>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ContactNumber>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ControlInformationIdentifier rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ControlInformationIdentifier>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ControlInformationIdentifier>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_DeclaredValueForCarriage rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<DeclaredValueForCarriage>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</DeclaredValueForCarriage>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_DeclaredValueForCustoms rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<DeclaredValueForCustoms>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</DeclaredValueForCustoms>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_DeclaredValueForInsurance rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<DeclaredValueForInsurance>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</DeclaredValueForInsurance>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_DepartureCode rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<DepartureCode>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</DepartureCode>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_DestinationCode rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<DestinationCode>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</DestinationCode>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_FreeText rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<FreeText>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</FreeText>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_HarmonisedCommodityCode rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<HarmonisedCommodityCode>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</HarmonisedCommodityCode>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_HWBSerialNumber rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<HWBSerialNumber>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</HWBSerialNumber>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_InformationIdentifier rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<InformationIdentifier>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</InformationIdentifier>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ADR rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ADR>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ADR>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_LOC rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<LOC>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</LOC>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_NAM rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<NAM>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</NAM>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ISOCountryCode rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ISOCountryCode>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ISOCountryCode>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ISOCurrencyCode rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ISOCurrencyCode>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ISOCurrencyCode>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_MBI rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<MBI>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</MBI>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_CNE rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<CNE>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</CNE>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_CVD rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<CVD>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</CVD>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_HBS rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<HBS>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</HBS>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_HTS rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<HTS>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</HTS>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_OCI rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<OCI>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</OCI>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_SHP rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<SHP>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</SHP>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_TXT rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<TXT>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</TXT>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ManifestDescriptionOfGoods rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ManifestDescriptionOfGoods>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ManifestDescriptionOfGoods>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_Name rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<Name>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</Name>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_NoCustomsValue rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<NoCustomsValue>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</NoCustomsValue>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_NoValue rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<NoValue>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</NoValue>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_NoValueDeclared rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<NoValueDeclared>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</NoValueDeclared>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_NumberOfPieces rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<NumberOfPieces>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</NumberOfPieces>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_OriginCode rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<OriginCode>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</OriginCode>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_PCIndOtherCharges rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<PCIndOtherCharges>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</PCIndOtherCharges>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_PCIndWeightValuation rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<PCIndWeightValuation>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</PCIndWeightValuation>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_Place rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<Place>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</Place>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_PostCode rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<PostCode>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</PostCode>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_ShipmentDescriptionCode rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<ShipmentDescriptionCode>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</ShipmentDescriptionCode>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_SLAC rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<SLAC>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</SLAC>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_SpecialHandlingCode rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<SpecialHandlingCode>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</SpecialHandlingCode>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_StateProvince rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<StateProvince>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</StateProvince>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_StreetAddress rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<StreetAddress>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</StreetAddress>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_SupplementaryControlInformation rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<SupplementaryControlInformation>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</SupplementaryControlInformation>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_Weight rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<Weight>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</Weight>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_WeightCode rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<WeightCode>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</WeightCode>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_CRLF rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<CRLF>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</CRLF>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_CR rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<CR>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</CR>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_LF rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<LF>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</LF>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_Slant rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<Slant>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</Slant>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_Hyphen rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<Hyphen>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</Hyphen>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_Alpha rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<Alpha>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</Alpha>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_Numeric rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<Numeric>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</Numeric>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_Decimal rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<Decimal>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</Decimal>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_Mixed rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<Mixed>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</Mixed>");
    terminal = false;
    return null;
  }

  public Object visit(Rule_Text rule)
  {
    if (!terminal) System.out.println();
    System.out.print("<Text>");
    terminal = false;
    visitRules(rule.rules);
    if (!terminal) System.out.println();
    System.out.print("</Text>");
    terminal = false;
    return null;
  }

  public Object visit(Terminal_StringValue value)
  {
    System.out.print(value.spelling);
    terminal = true;
    return null;
  }

  public Object visit(Terminal_NumericValue value)
  {
    System.out.print(value.spelling);
    terminal = true;
    return null;
  }

  protected Boolean visitRules(ArrayList<Rule> rules)
  {
    for (Rule rule : rules)
      rule.accept(this);
    return null;
  }
}

/* -----------------------------------------------------------------------------
 * eof
 * -----------------------------------------------------------------------------
 */
