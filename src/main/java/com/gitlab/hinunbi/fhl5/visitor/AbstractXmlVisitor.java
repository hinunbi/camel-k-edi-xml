package com.gitlab.hinunbi.fhl5.visitor;

import com.gitlab.hinunbi.fhl5.parser.*;
import com.gitlab.hinunbi.xml.PrettyPrintXml;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import javax.xml.transform.TransformerException;
import java.util.ArrayList;

@Data
public abstract class AbstractXmlVisitor implements Visitor {

  private PrettyPrintXml prettyPrintXml = new PrettyPrintXml();
  private boolean terminal = true;
  private int depth = 0;
  private int indent = 2;
  private boolean isParentTag = false;
  private StringBuilder result = new StringBuilder();

  protected Object visitWithTag(Rule rule, String tag) {

    String format = String.format("<%s>",  tag);
    result.append(format);

    terminal = false;
    isParentTag = true;
    visitRules(rule.rules);
    format = String.format("</%s>", tag);
    result.append(format);
    terminal = false;
    return null;
  }

  protected Object visitWithoutTagAndValue(Rule rule, String tag) {
    return null;
  }

  protected void visitRules(ArrayList<Rule> rules) {
    for (Rule rule : rules) {
      rule.accept(this);
    }
  }

  protected Object visitWithoutLine(Rule rule) {
    terminal = false;
    visitRules(rule.rules);
    terminal = false;
    return null;
  }

  public String getXml() throws TransformerException {
    String xml;
    xml = result.toString();
    return prettyPrintXml.prettify(xml);
  }

  @Override
  public String toString() {
    return result.toString();
  }

  @Override
  public Object visit(Rule_Hyphen rule) {
    return visitWithoutTagAndValue(rule, "-");
  }

  @Override
  public Object visit(Rule_CRLF rule) {
    return visitWithoutTagAndValue(rule, "CRLF");
  }

  @Override
  public Object visit(Rule_Slant rule) {
    return visitWithoutTagAndValue(rule, "Slant");
  }

  @Override
  public Object visit(Rule_Alpha rule) {
    return visitWithoutLine(rule);
  }

  @Override
  public Object visit(Rule_Numeric rule) {
    return visitWithoutLine(rule);
  }

  @Override
  public Object visit(Rule_Decimal rule) {
    return visitWithoutLine(rule);
  }

  @Override
  public Object visit(Rule_Text rule) {
    return visitWithoutLine(rule);
  }

  @Override
  public Object visit(Rule_Mixed rule) {
    return visitWithoutLine(rule);
  }

  @Override
  public Object visit(Rule_CR rule) {
    return visitWithoutTagAndValue(rule, "CR");
  }

  @Override
  public Object visit(Rule_LF rule) {
    return visitWithoutTagAndValue(rule, "LF");
  }

  @Override
  public Object visit(Terminal_StringValue value) {
    if (isParentTag) {
      String format = StringUtils.repeat(StringUtils.SPACE, (depth - 1) * indent);
      result.append(format);
      isParentTag = false;
    }
    result.append(value.spelling);
    terminal = true;
    return null;
  }

  @Override
  public Object visit(Terminal_NumericValue value) {
    result.append(value.spelling);
    terminal = true;
    return null;
  }
}
