package com.gitlab.hinunbi.fhl5.visitor;

import com.gitlab.hinunbi.fhl5.parser.*;

public class XmlVisitor extends AbstractXmlVisitor {

  public Object visit(Rule_StandardMessageIdentification rule) {
    return visitWithTag(rule, "StandardMessageIdentification");
  }

  public Object visit(Rule_AWBSerialNumber rule) {
    return visitWithTag(rule, "AWBSerialNumber");
  }

  public Object visit(Rule_AirlinePrefix rule) {
    return visitWithTag(rule, "AirlinePrefix");
  }

  public Object visit(Rule_FHL5 rule) {
    return visitWithTag(rule, "FHL5");
  }

  public Object visit(Rule_MasterAWBConsignmentDetail rule) {
    return visitWithTag(rule, "MasterAWBConsignmentDetail");
  }

  public Object visit(Rule_MasterAWBIdentification rule) {
    return visitWithTag(rule, "MasterAWBIdentification");
  }

  public Object visit(Rule_AWBOriginAndDestination rule) {
    return visitWithTag(rule, "AWBOriginAndDestination");
  }

  public Object visit(Rule_QuantityDetail rule) {
    return visitWithTag(rule, "QuantityDetail");
  }

  public Object visit(Rule_HousWaybillSummaryDetails rule) {
    return visitWithTag(rule, "HousWaybillSummaryDetails");
  }

  public Object visit(Rule_HouseWaybillOriginAndDestination rule) {
    return visitWithTag(rule, "HouseWaybillOriginAndDestination");
  }

  public Object visit(Rule_HouseWaybillTotals rule) {
    return visitWithTag(rule, "HouseWaybillTotals");
  }

  public Object visit(Rule_NatureOfGoods rule) {
    return visitWithTag(rule, "NatureOfGoods");
  }

  public Object visit(Rule_SpecialHandlingRequirements rule) {
    return visitWithTag(rule, "SpecialHandlingRequirements");
  }

  public Object visit(Rule_FreeTextDescriptionOfGoods rule) {
    return visitWithTag(rule, "FreeTextDescriptionOfGoods");
  }

  public Object visit(Rule_HarmonisedTariffScheduleInformation rule) {
    return visitWithTag(rule, "HarmonisedTariffScheduleInformation");
  }

  public Object visit(Rule_OtherInformation rule) {
    return visitWithTag(rule, "OtherInformation");
  }

  public Object visit(Rule_ShipperNameAndAddress rule) {
    return visitWithTag(rule, "ShipperNameAndAddress");
  }

  public Object visit(Rule_ShipperName rule) {
    return visitWithTag(rule, "ShipperName");
  }

  public Object visit(Rule_ShipperStreetAddress rule) {
    return visitWithTag(rule, "ShipperStreetAddress");
  }

  public Object visit(Rule_ShipperLocation rule) {
    return visitWithTag(rule, "ShipperLocation");
  }

  public Object visit(Rule_ShipperCodedLocation rule) {
    return visitWithTag(rule, "ShipperCodedLocation");
  }

  public Object visit(Rule_ShipperContactDetail rule) {
    return visitWithTag(rule, "ShipperContactDetail");
  }

  public Object visit(Rule_ConsigneeNameAndAddress rule) {
    return visitWithTag(rule, "ConsigneeNameAndAddress");
  }

  public Object visit(Rule_ConsigneeName rule) {
    return visitWithTag(rule, "ConsigneeName");
  }

  public Object visit(Rule_ConsigneeStreetAddress rule) {
    return visitWithTag(rule, "ConsigneeStreetAddress");
  }

  public Object visit(Rule_ConsigneeLocation rule) {
    return visitWithTag(rule, "ConsigneeLocation");
  }

  public Object visit(Rule_ConsigneeCodedLocation rule) {
    return visitWithTag(rule, "ConsigneeCodedLocation");
  }

  public Object visit(Rule_ConsigneeContactDetail rule) {
    return visitWithTag(rule, "ConsigneeContactDetail");
  }

  public Object visit(Rule_ChargeDeclarations rule) {
    return visitWithTag(rule, "ChargeDeclarations");
  }

  public Object visit(Rule_PrepaidCollectChargeDeclarations rule) {
    return visitWithTag(rule, "PrepaidCollectChargeDeclarations");
  }

  public Object visit(Rule_ValueForCarriageDeclaration rule) {
    return visitWithTag(rule, "ValueForCarriageDeclaration");
  }

  public Object visit(Rule_ValueForCustomsDeclaration rule) {
    return visitWithTag(rule, "ValueForCustomsDeclaration");
  }

  public Object visit(Rule_ValueForInsuranceDeclaration rule) {
    return visitWithTag(rule, "ValueForInsuranceDeclaration");
  }

  public Object visit(Rule_ContactIdentifier rule) {
    return visitWithTag(rule, "ContactIdentifier");
  }

  public Object visit(Rule_ContactNumber rule) {
    return visitWithTag(rule, "ContactNumber");
  }

  public Object visit(Rule_ControlInformationIdentifier rule) {
    return visitWithTag(rule, "ControlInformationIdentifier");
  }

  public Object visit(Rule_DeclaredValueForCarriage rule) {
    return visitWithTag(rule, "DeclaredValueForCarriage");
  }

  public Object visit(Rule_DeclaredValueForCustoms rule) {
    return visitWithTag(rule, "DeclaredValueForCustoms");
  }

  public Object visit(Rule_DeclaredValueForInsurance rule) {
    return visitWithTag(rule, "DeclaredValueForInsurance");
  }

  public Object visit(Rule_DepartureCode rule) {
    return visitWithTag(rule, "DepartureCode");
  }

  public Object visit(Rule_DestinationCode rule) {
    return visitWithTag(rule, "DestinationCode");
  }

  public Object visit(Rule_FreeText rule) {
    return visitWithTag(rule, "FreeText");
  }

  public Object visit(Rule_HarmonisedCommodityCode rule) {
    return visitWithTag(rule, "HarmonisedCommodityCode");
  }

  public Object visit(Rule_HWBSerialNumber rule) {
    return visitWithTag(rule, "HWBSerialNumber");
  }

  public Object visit(Rule_InformationIdentifier rule) {
    return visitWithTag(rule, "InformationIdentifier");
  }

  public Object visit(Rule_ADR rule) {
    return visitWithTag(rule, "ADR");
  }

  public Object visit(Rule_LOC rule) {
    return visitWithTag(rule, "LOC");
  }

  public Object visit(Rule_NAM rule) {
    return visitWithTag(rule, "NAM");
  }

  public Object visit(Rule_ISOCountryCode rule) {
    return visitWithTag(rule, "ISOCountryCode");
  }

  public Object visit(Rule_ISOCurrencyCode rule) {
    return visitWithTag(rule, "ISOCurrencyCode");
  }

  public Object visit(Rule_MBI rule) {
    return visitWithTag(rule, "MBI");
  }

  public Object visit(Rule_CNE rule) {
    return visitWithTag(rule, "CNE");
  }

  public Object visit(Rule_CVD rule) {
    return visitWithTag(rule, "CVD");
  }

  public Object visit(Rule_HBS rule) {
    return visitWithTag(rule, "HBS");
  }

  public Object visit(Rule_HTS rule) {
    return visitWithTag(rule, "HTS");
  }

  public Object visit(Rule_OCI rule) {
    return visitWithTag(rule, "OCI");
  }

  public Object visit(Rule_SHP rule) {
    return visitWithTag(rule, "SHP");
  }

  public Object visit(Rule_TXT rule) {
    return visitWithTag(rule, "TXT");
  }

  public Object visit(Rule_ManifestDescriptionOfGoods rule) {
    return visitWithTag(rule, "ManifestDescriptionOfGoods");
  }

  public Object visit(Rule_Name rule) {
    return visitWithTag(rule, "Name");
  }

  public Object visit(Rule_NoCustomsValue rule) {
    return visitWithTag(rule, "NoCustomsValue");
  }

  public Object visit(Rule_NoValue rule) {
    return visitWithTag(rule, "NoValue");
  }

  public Object visit(Rule_NoValueDeclared rule) {
    return visitWithTag(rule, "NoValueDeclared");
  }

  public Object visit(Rule_NumberOfPieces rule) {
    return visitWithTag(rule, "NumberOfPieces");
  }

  public Object visit(Rule_OriginCode rule) {
    return visitWithTag(rule, "OriginCode");
  }

  public Object visit(Rule_PCIndOtherCharges rule) {
    return visitWithTag(rule, "PCIndOtherCharges");
  }

  public Object visit(Rule_PCIndWeightValuation rule) {
    return visitWithTag(rule, "PCIndWeightValuation");
  }

  public Object visit(Rule_Place rule) {
    return visitWithTag(rule, "Place");
  }

  public Object visit(Rule_PostCode rule) {
    return visitWithTag(rule, "PostCode");
  }

  public Object visit(Rule_ShipmentDescriptionCode rule) {
    return visitWithTag(rule, "ShipmentDescriptionCode");
  }

  public Object visit(Rule_SLAC rule) {
    return visitWithTag(rule, "SLAC");
  }

  public Object visit(Rule_SpecialHandlingCode rule) {
    return visitWithTag(rule, "SpecialHandlingCode");
  }

  public Object visit(Rule_StateProvince rule) {
    return visitWithTag(rule, "StateProvince");
  }

  public Object visit(Rule_StreetAddress rule) {
    return visitWithTag(rule, "StreetAddress");
  }

  public Object visit(Rule_SupplementaryControlInformation rule) {
    return visitWithTag(rule, "SupplementaryControlInformation");
  }

  public Object visit(Rule_Weight rule) {
    return visitWithTag(rule, "Weight");
  }

  public Object visit(Rule_WeightCode rule) {
    return visitWithTag(rule, "WeightCode");
  }
}
