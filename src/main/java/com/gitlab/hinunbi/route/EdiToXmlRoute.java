package com.gitlab.hinunbi.route;

// camel-k: language=java dependency=camel-openapi-java
// camel-k: dependency=mvn:com.gitlab.hinunbi/camel-k-edi-xml:0.0.8


import com.gitlab.hinunbi.translator.Fhl5EdiToXml;
import org.apache.camel.builder.endpoint.EndpointRouteBuilder;

public class EdiToXmlRoute extends EndpointRouteBuilder {

  @Override
  public void configure() {

    from(direct("translate"))
        .routeId("ediRoute")
        .choice()
        .when(header("edi").contains("fhl5"))
        .process(new Fhl5EdiToXml())
        .log(">>>> \n${body}")
        .end();
  }
}
