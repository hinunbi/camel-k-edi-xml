package com.gitlab.hinunbi.translator;

import com.gitlab.hinunbi.fhl5.parser.Parser;
import com.gitlab.hinunbi.fhl5.parser.Rule;
import com.gitlab.hinunbi.fhl5.visitor.XmlVisitor;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;

public class Fhl5EdiToXml implements Processor {

  @Override
  public void process(Exchange exchange) throws Exception {

    Message in = exchange.getIn();
    String message = in.getBody(String.class);

    try {
      Rule rule = Parser.parse("FHL5", message);
      XmlVisitor fhl5XmlVisitor = new XmlVisitor();
      rule.accept(fhl5XmlVisitor);
      String data = fhl5XmlVisitor.getXml();
      in.setBody(data);
    } finally {
    }
  }
}
