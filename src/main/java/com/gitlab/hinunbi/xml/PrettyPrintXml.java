package com.gitlab.hinunbi.xml;

import org.xml.sax.InputSource;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public class PrettyPrintXml {

  public String prettify(String sourceXml) throws TransformerException {

    Transformer transformer = SAXTransformerFactory.newInstance().newTransformer();

    transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, "yes");
    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

    Source xmlSource = new SAXSource(new InputSource(new ByteArrayInputStream(sourceXml.getBytes())));
    StreamResult res = new StreamResult(new ByteArrayOutputStream());

    transformer.transform(xmlSource, res);

    return new String(((ByteArrayOutputStream) res.getOutputStream()).toByteArray());
  }
}
