package com.gitlab.hinunbi;

import com.gitlab.hinunbi.route.EdiToXmlRoute;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Test;
import org.xmlunit.matchers.CompareMatcher;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class EdiToXmlRouteTest extends CamelTestSupport {

  @Override
  protected RoutesBuilder createRouteBuilder() {
    return new EdiToXmlRoute();
  }

  @Test
  public void testFhl5Edi() throws IOException {

    File fhl5Edi = new File("sample/fhl5.edi");

    String result = template.requestBodyAndHeader(
        "direct:translate", fhl5Edi, "edi", "fhl5", String.class);
    System.out.println(result);

    byte[] encoded = Files.readAllBytes(Paths.get("sample/fhl5.xml"));
    String expected = new String(encoded, StandardCharsets.UTF_8);

    org.hamcrest.MatcherAssert.assertThat(result, CompareMatcher.isIdenticalTo(expected));
  }
}